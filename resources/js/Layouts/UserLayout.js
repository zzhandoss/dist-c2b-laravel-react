import React, {createContext, useState} from "react";
import {Link} from "@inertiajs/inertia-react";
import Logo from 'C:/OpenServer/domains/dist.c2b.local/storage/app/public/images/CerebroLogo1.png';
import AsideFooter from "@/Components/AsideFooter";
import HeaderProfile from "@/Components/HeaderProfile";

export const AsideContext = createContext(false)

export default function UserLayout({auth,header,nav,notification,favorite,children}){
    const [asidePositionOpenedState, setAsidePositionOpenedState] = useState(false)


    function handleAsideMouseEnter(e){
        setAsidePositionOpenedState(true)
    }
    function handleAsideMouseLeave(e){
        setAsidePositionOpenedState(false)
    }

    return(
        <AsideContext.Provider value={asidePositionOpenedState}>
            <div className="min-h-screen min-w-screen bg-gray-100 relative flex">
                <aside
                    onMouseEnter={event=>handleAsideMouseEnter(event)}
                    onMouseLeave={event => handleAsideMouseLeave(event)}
                    className="relative min-w-[72px] w-[72px] max-w-[312px] transition-all duration-500 text-gray-400 min-h-screen max-h-screen bg-slate-800 flex flex-col overflow-hidden sticky top-0 hover:w-[16rem] hover:min-w-[16rem] hover:transition-all hover:duration-500"
                >
                    <div className="w-full h-24 bg-slate-800 flex">
                        <img src={Logo} alt="Cerebro Innovation Technologies" className={`p-4 min-w-[16rem] w-[16rem] h-20` } />
                    </div>
                    <div className="w-full p-2 flex flex-col h-full">
                        {nav}
                    </div>
                    <AsideFooter />
                </aside>
                <div className="flex w-full min-h-screen flex-col">
                    <header className="flex w-full h-20 bg-white flex-row shadow-sm p-2 md:p-4 md:pl-12 md:pr-12 items-center">
                        <div className="flex w-full flex-row">
                            {header}
                        </div>
                        <div className="flex flex-row">
                            {
                                notification && notification
                            }
                            {
                                favorite && favorite
                            }
                            <HeaderProfile user={auth.user} />
                        </div>
                    </header>
                    <main>{children}</main>
                </div>

            </div>
        </AsideContext.Provider>
    )
}


