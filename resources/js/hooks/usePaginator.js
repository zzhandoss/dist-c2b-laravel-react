import React, {useMemo} from 'react';

const usePaginator = (paginator) => {
    const pagin = []
    return useMemo(() => {
        const last = paginator.last_page
        const c = paginator.current_page
        const d = 1
        const a = c - d
        const b = c + d
        let isHasDotsForward = false
        let isHasDotsBackward = false
        if(paginator.links) {
            pagin.push({...paginator.links[0],page_number:c-1})
            paginator.links.map((link,index) => {
                if(last > 7) {
                    if(!isHasDotsBackward){
                        if((index < c && index < a && index > 1)){
                            pagin.push({url: null,label:'...',disabled: true})
                            isHasDotsBackward = true
                        }
                    }
                    if(!isHasDotsForward){
                        if((index > 0 && index > b && index < last - d && index!==last+1)){
                            pagin.push({url: null,label:'...',disabled: true})
                            isHasDotsForward = true
                        }
                    }

                    (
                        index === 1
                        || index === c
                        || index === last
                        || (index >= last - d && last - d >= b && last - d !== b + 1 && index !== last + 1)
                        || (index > c && index <= b && index !== b+1 )
                        || (index < c && index >= a && index !== a-1 && index !== 0)
                    )
                    && index !== 0 && index !== last+1
                    && pagin.push(link)
                } else {
                    if(index !== 0 && index !== last+1)
                        pagin.push(link)
                }
            })
            pagin.push({...paginator.links[last+1],page_number:c+1})
            return pagin
        }
    },[paginator])
};

export default usePaginator;
