import React, {useEffect, useMemo} from 'react';

const useDropdownListSuggestions = (q,items,selectedItems,setSuggestionsList) => {
    const suggestions = useMemo(() => {
        return (items && items.filter(item => (item.name.toLowerCase().indexOf(q.toLowerCase()) > -1) && selectedItems.filter(tag => tag === item).length < 1 )) ?? []
    },[items,q,selectedItems])
    useEffect(() => {
        setSuggestionsList(suggestions)
    },[suggestions])
    return suggestions
};

export default useDropdownListSuggestions;
