import React, {useCallback, useMemo, useState} from 'react';
import {Inertia} from "@inertiajs/inertia";
import usePaginator from "@/hooks/usePaginator";

const useSearchOrders = (q,filters,setOrders) => {
    const [isLoading,setIsLoading] = useState(false)
    const [paginator,setPaginator] = useState({})
    const isFiltersPresents = useMemo(() => {
        return filters.categories.length > 0 || filters.date !== '' || filters.quantity > 0
    },[filters])

    const search = useCallback((href,pageNumber) => {
        Inertia.get(route(href),{q: q,filters: isFiltersPresents && JSON.stringify(filters),page: pageNumber ?? 1}, {
            preserveState: true,
            replace: true,
            only: ['orders'],
            onBefore: () => {
                setIsLoading(true)
            },
            onSuccess: (data) => {
                console.log(data)
                setOrders(data.props.orders.data)
                setPaginator({...data.props.orders, data:''})
                setIsLoading(false)
            }
        })
    },[q,filters,isFiltersPresents])

    const paginatorInfo = usePaginator(paginator)

    return [search,isLoading,paginatorInfo]
};

export default useSearchOrders;

