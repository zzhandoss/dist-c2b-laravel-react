import {useMemo} from "react"

export const useFilteredOrders = (inputOrders,filters) => {
    return useMemo(() => {
        return inputOrders ? inputOrders.filter(order =>
                (filters.categories.length > 0
                    ? filters.categories.includes(filters.categories.find(category => category.id === order.category_id))
                    : true)
            )
            : []
    }, [filters, inputOrders])
}

export const useSearchedOrders = (orders,filters,q) => {
    const filteredOrders = useFilteredOrders(orders,filters)
    const columns = filteredOrders[0] && Object.keys(filteredOrders[0])

    return useMemo(() => {
        return filteredOrders.filter((order) =>
                columns.some((col) =>
                        order[col]
                        ? order[col].toString().toLowerCase().indexOf(q.toLowerCase()) > -1
                        : false
                )
    )}
    ,[filteredOrders,q])
}


