import React, {useContext, useEffect} from 'react';
import UserLayout from "@/Layouts/UserLayout";
import { Head } from '@inertiajs/inertia-react';
import AsideNavbar from "@/Components/Customer/AsideNavbar";

export default function CustomerDashboard() {
    return (
        <>
            <Head title="CustomerDashboard" >
                <title>Входящие</title>
            </Head>
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200">Я ОЗ</div>
                    </div>
                </div>
            </div>
        </>
    );
}



CustomerDashboard.layout = page => {
    return (
        <UserLayout
            children = {page}
            auth = {page.props.auth}
            errors = {page.props.errors}
            header = {  <>
                        <h2 className="font-semibold text-xl text-gray-800 leading-tight">Входящие предложения</h2>
                        </>
            }
            nav = {
                <>
                    <AsideNavbar />
                </>
            }
            notification={
                <div className="min-w-[50px] justify-around text-sm text-gray-400 flex flex-row items-center">
                    <span className="material-icons-outlined">notifications</span>
                </div>
            }
        />
    )
}
