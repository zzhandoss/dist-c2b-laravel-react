import React, { useEffect } from 'react';
import Button from '@/Components/Button';
import Guest from '@/Layouts/Guest';
import Input from '@/Components/Input';
import Label from '@/Components/Label';
import ValidationErrors from '@/Components/ValidationErrors';
import RadioButton from "@/Components/RadioButton";
import { Head, Link, useForm } from '@inertiajs/inertia-react';
import {value} from "lodash/seq";

export default function Register() {
    const { data, setData, post, processing, errors, reset } = useForm({
        name: '',
        surname: '',
        patronymic: '',
        email: '',
        company: '',
        company_position: '',
        password: '',
        password_confirmation: '',
        role: 'customer',
    });

    useEffect(() => {
        const radioButtonCustomer = document.getElementById('radio-customer')
        radioButtonCustomer.checked = true

        return () => {
            reset('password', 'password_confirmation');
        };
    }, []);

    const onHandleChange = (event) => {
        setData(event.target.name, (event.target.type === 'checkbox') ? event.target.checked : event.target.value);
    };

    const onHandleRadioChange = event => {
        setData('role', event.target.value)
    }

    const submit = (e) => {
        e.preventDefault();
        post(route('register'));
    };

    return (
        <Guest>
            <Head title="Регистрация" />

            <ValidationErrors errors={errors} />

            <div className="w-auto h-9  text-gray-900 font-bold">
                Регистрация
            </div>

            <form onSubmit={submit}>
                <div>
                    <Label forInput="name" value="Имя" />

                    <Input
                        type="text"
                        name="name"
                        value={data.name}
                        className="mt-1 block w-full"
                        autoComplete="name"
                        isFocused={true}
                        handleChange={onHandleChange}
                        required
                    />
                </div>

                <div className="mt-4">
                    <Label forInput="surname" value="Фамилия" />

                    <Input
                        type="text"
                        name="surname"
                        value={data.surname}
                        className="mt-1 block w-full"
                        autoComplete="surname"
                        handleChange={onHandleChange}
                        required
                    />
                </div>

                <div className="mt-4">
                    <Label forInput="patronymic" value="Отчество" />

                    <Input
                        type="text"
                        name="patronymic"
                        value={data.patronymic}
                        className="mt-1 block w-full"
                        autoComplete="patronymic"
                        handleChange={onHandleChange}
                        required
                    />
                </div>

                <div className="mt-4">
                    <Label forInput="role" value="Роль" />
                    <div className="flex justify-between">
                        <RadioButton
                            name="role"
                            value="customer"
                            className="mt-1 mr-1"
                            label="Оператор-заказчик"
                            isFocused={false}
                            handleChange={onHandleRadioChange}
                            required
                        />
                        <RadioButton
                            name="role"
                            value="distributor"
                            className="mt-1 mr-1"
                            label="Дистрибьютер"
                            isFocused={false}
                            handleChange={onHandleRadioChange}
                            required

                        />
                    </div>

                </div>

                <div className="mt-4">
                    <Label forInput="company" value="Организация" />

                    <Input
                        type="text"
                        name="company"
                        value={data.company}
                        className="mt-1 block w-full"
                        autoComplete="company"
                        handleChange={onHandleChange}
                        required
                    />
                </div>

                <div className="mt-4">
                    <Label forInput="company_position" value="Должность" />

                    <Input
                        type="text"
                        name="company_position"
                        value={data.company_position}
                        className="mt-1 block w-full"
                        autoComplete="company_position"
                        handleChange={onHandleChange}
                        required
                    />
                </div>

                <div className="mt-4">
                    <Label forInput="email" value="Эл. адрес" />

                    <Input
                        type="email"
                        name="email"
                        value={data.email}
                        className="mt-1 block w-full"
                        autoComplete="username"
                        handleChange={onHandleChange}
                        required
                    />
                </div>

                <div className="mt-4">
                    <Label forInput="password" value="Пароль" />

                    <Input
                        type="password"
                        name="password"
                        value={data.password}
                        className="mt-1 block w-full"
                        autoComplete="new-password"
                        handleChange={onHandleChange}
                        required
                    />
                </div>

                <div className="mt-4">
                    <Label forInput="password_confirmation" value="Подтверждение пароля" />

                    <Input
                        type="password"
                        name="password_confirmation"
                        value={data.password_confirmation}
                        className="mt-1 block w-full"
                        handleChange={onHandleChange}
                        required
                    />
                </div>

                <div className="flex items-center justify-end mt-4">
                    <Link href={route('login')} className="underline text-sm text-gray-600 hover:text-gray-900">
                        Уже есть аккаунт?
                    </Link>

                    <Button className="ml-4" processing={processing}>
                        Зарегистрироваться
                    </Button>
                </div>
            </form>
        </Guest>
    );
}
