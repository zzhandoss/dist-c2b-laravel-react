import React, {useEffect, useMemo, useState} from 'react';
import { Head } from '@inertiajs/inertia-react';

import UserLayout from "@/Layouts/UserLayout";
import AsideNavbar from "@/Components/Distributor/AsideNavbar";

export default function DistributorDashboard(props) {

    return (
        <>
            <Head title="CustomerOut" ><title>Future title</title></Head>
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 flex flex-col">
                    Distribute
                </div>
            </div>
        </>

    );
}

DistributorDashboard.layout = (page) => (
    <UserLayout
        children = {page}
        auth={page.props.auth}
        errors={page.props.errors}
        header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Мои предложения</h2>}
        nav={
            <>
                <AsideNavbar />
            </>
        }
        favorite={
            <div className="min-w-[50px] justify-around text-sm text-gray-400 flex flex-row items-center">
                <span className="material-symbols-outlined">favorite</span>
            </div>
        }
    />
)
