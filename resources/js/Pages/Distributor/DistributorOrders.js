import React, {useEffect, useState} from 'react';
import { Head } from '@inertiajs/inertia-react';

import UserLayout from "@/Layouts/UserLayout";
import AsideNavbar from "@/Components/Distributor/AsideNavbar";
import useSearchOrders from "@/hooks/useSearchOrders";
import OrderSearch from "@/Components/OrderSearch";
import Orders from "@/Components/Distributor/Orders";
import Paginator from "@/Components/UI/Paginator/Paginator";
import LoadingSpinner from "@/Components/UI/Spinner/LoadingSpinner";

let debouncedSearch
export default function DistributorOrders(props) {
    const href = 'distributor_orders'
    const [orders,setOrders] = useState([])
    const [q,setQ] = useState('')

    const [filters,setFilters] = useState({
        categories: [],
        date: '',
        quantity: 0,
    })

    const [search,isLoading,paginator] = useSearchOrders(q,filters,setOrders)

    const getQuery = (query) => {
        query ? setQ(query) : setQ('')
    }
    useEffect(() => {
        clearTimeout(debouncedSearch)
        debouncedSearch = setTimeout(()=>search(href),300)
    },[search])

    return (
        <>
            <Head title="DistributorOrders" ><title>Заявки от заказчиков</title></Head>
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 flex flex-col">
                    <div className="search-container flex">
                        <OrderSearch
                            query={q}
                            setQuery={getQuery}
                            filters={filters}
                            setFilters={setFilters}
                            categories={props.categories}
                            className="w-11/12 mx-auto"
                            filtersClassName="p-2"
                        />
                    </div>
                    {
                        !isLoading
                            ?
                            <div className="order-container w-full flex flex-col p-2">
                                {
                                    orders.length > 0
                                        ?
                                        <>
                                            {
                                                <Orders
                                                    orders={props.orders.data}
                                                />
                                            }
                                            <Paginator
                                                paginator={paginator}
                                                to={props.orders.to}
                                                from={props.orders.from}
                                                total={props.orders.total}
                                                href={href}
                                                search={search}
                                            />
                                        </>
                                        :
                                        <div className="order w-11/12 mx-auto  h-auto p-4 mt-2 flex justify-between flex-wrap bg-white rounded-md shadow-md ">Нет результатов</div>
                                }
                            </div>
                            :
                            <div className="w-full min-h-[100px] justify-center items-center flex flex-col mx-auto">
                                <LoadingSpinner />
                            </div>
                    }

                </div>
            </div>
        </>

    );
}

DistributorOrders.layout = (page) => (
    <UserLayout
        children = {page}
        auth={page.props.auth}
        errors={page.props.errors}
        header={<h2 className="font-semibold  text-base sm:text-lg md:text-xl text-gray-800 leading-tight">Заявки от операторов</h2>}
        nav={
            <>
                <AsideNavbar />
            </>
        }
        favorite={
            <div className="min-w-[50px] justify-around text-sm text-gray-400 flex flex-row items-center">
                <span className="material-symbols-outlined">favorite</span>
            </div>
        }
    />
)
