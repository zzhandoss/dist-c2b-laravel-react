import React, {useState} from 'react';
import UserLayout from "@/Layouts/UserLayout";
import AsideNavbar from "@/Components/Distributor/AsideNavbar";
import {Head, useForm} from "@inertiajs/inertia-react";
import OrderCard from "@/Components/Distributor/OrderCard";
import FileUpload from "@/Components/UI/FileDragAndDrop/FileUpload";
import Button from "@/Components/Button";
import Proposals from "@/Components/Distributor/ProposalPage/Proposals";

const DistributorProposalCreate = ({order,proposals},...props) => {
    const [file,setFile] = useState([])
    console.log(proposals)
    const {data,setData,post,progress} = useForm({
        order_id: order.id,
        files: []
    })

    function handleUploadButtonClick(e){
        post(route('distributor_proposal.store'),{
            onSuccess: () => {
                setData({...data,files: []})
                setFile([])
            }
        })
    }
    function fileCallback(files){
        setFile(files)
        setData({...data,files:files})
    }
    return (
        <>
            <Head title="DistributorOrders" ><title>Новое предложение</title></Head>
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8 flex flex-col">
                    <OrderCard order={order} />
                    <div className="w-11/12 mx-auto min-h-[150px] transition-all p-4 my-6 mb-6 flex flex-col flex-wrap bg-white rounded-md shadow-md relative">
                        <div className="flex flex-row border-b">
                            <span className="text-lg text-gray-500 font-bold">Загрузка файла</span>
                        </div>
                        <FileUpload
                            savedFiles={file}
                            uploadFileCb={fileCallback}
                            count={5}
                            formats={['pdf','doc','docx','txt']} />
                        <div className="flex mt-2 mr-2 justify-end">
                            <Button  processing={progress} onClick={e => handleUploadButtonClick(e)}>
                                {
                                    progress
                                    ?
                                        progress.percentage
                                    :
                                        "Отправить"
                                }
                            </Button>
                        </div>
                    </div>
                    <Proposals proposals={proposals} />
                </div>
            </div>
        </>
    );
};

DistributorProposalCreate.layout = (page) => (
    <UserLayout
        children = {page}
        auth={page.props.auth}
        errors={page.props.errors}
        header={<h2 className="font-semibold text-sm md:text-xl text-gray-800 leading-tight">Новое предложение</h2>}
        nav={
            <>
                <AsideNavbar />
            </>
        }
        favorite={
            <div className="min-w-[50px] justify-around text-sm text-gray-400 flex flex-row items-center">
                <span className="material-symbols-outlined">favorite</span>
            </div>
        }
    />
)

export default DistributorProposalCreate;
