import React, {useEffect} from 'react';
import UserLayout from "@/Layouts/UserLayout";
import {Head, useForm} from '@inertiajs/inertia-react';
import NavLink from "@/Components/NavLink";
import AsideNavbar from "@/Components/Customer/AsideNavbar";
import ValidationErrors from "@/Components/ValidationErrors";
import Label from "@/Components/Label";
import Input from "@/Components/Input";
import Button from "@/Components/Button";
import Textarea from "@/Components/Textarea"



export default function CreateOrder(props) {
    const uom = [
        {
            id: 1,
            name: 'шт'
        },
        {
            id: 2,
            name: 'км'
        },
        {
            id: 3,
            name: 'л'
        },
        {
            id: 4,
            name: 'м'
        },
        {
            id: 5,
            name: 'мл'
        }
    ]

    const { data, setData, post, processing, errors, reset } = useForm({
        order_number: '',
        order_name: '',
        lot_number: '',
        lot_name: '',
        lot_category_id: 0,
        lot_description: '',
        lot_additional_description: '',
        quantity: '',
        unit_of_measure: 0,
        delivery_address: '',
        delivery_date: '',
        characteristics: ''
    });


    useEffect(() => {

    }, []);

    const onHandleChange = (event) => {
        setData(event.target.name, (event.target.type === 'checkbox') ? event.target.checked : event.target.value);
    };

    const submit = (e) => {
        e.preventDefault();
        console.log(data)
        post(route('order.store'));
    };

    return (
        <>
            <Head title="CreateOrder" ><title>Новая заявка</title></Head>
            <div className="py-12">
                <div className="max-w-7xl mx-auto sm:px-6 lg:px-8">
                    <div className="bg-white overflow-hidden shadow-sm sm:rounded-lg">
                        <div className="p-6 bg-white border-b border-gray-200">
                            <ValidationErrors errors={errors} />
                            <form onSubmit={submit}>
                                <div>
                                    <Label forInput="order_number" value="Номер закупки" />

                                    <Input
                                        type="number"
                                        name="order_number"
                                        value={data.order_number}
                                        className="mt-1 block w-full"
                                        isFocused={true}
                                        handleChange={onHandleChange}

                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="order_name" value="Наименование закупки" />

                                    <Input
                                        type="text"
                                        name="order_name"
                                        value={data.order_name}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}

                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="lot_number" value="Номер лота" />

                                    <Input
                                        type="number"
                                        name="lot_number"
                                        value={data.lot_number}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}

                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="lot_name" value="Наименование лота" />

                                    <Input
                                        type="text"
                                        name="lot_name"
                                        value={data.lot_name}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}
                                        required
                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="lot_category_id" value="Категория лота" />

                                    <select name="lot_category_id" id="lot_category_id" value={data.lot_category_id} required={true} onChange={onHandleChange} className={`mt-1 block w-full border-gray-300 focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 rounded-md shadow-sm `}>
                                        <option value={0} disabled>
                                            Выберите категорию
                                        </option>
                                        {
                                            props.categories.map(item => {
                                                return (
                                                    <option value={item.id} key={item.id}>
                                                        {item.name}
                                                    </option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                                <div className="mt-4">
                                    <Label forInput="lot_description" value="Описание лота" />

                                    <Textarea
                                        name="lot_description"
                                        value={data.lot_description}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}
                                        required
                                        cols="3"
                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="lot_additional_description" value="Дополнительное описание лота" />

                                    <Textarea
                                        name="lot_additional_description"
                                        value={data.lot_additional_description}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}

                                        cols="3"
                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="quantity" value="Количество" />

                                    <Input
                                        type="number"
                                        name="quantity"
                                        value={data.quantity}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}
                                        required
                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="unit_of_measure" value="Единица измерения" />

                                    <select name="unit_of_measure" id="unit_of_measure" value={data.unit_of_measure} required={true} onChange={onHandleChange} className={`mt-1 block w-full border-gray-300 focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 rounded-md shadow-sm `}>
                                        <option value={0} disabled>
                                            Выберите единицу измерения
                                        </option>
                                        {
                                            uom.map(item => {
                                                return (
                                                    <option value={item.name} key={item.id}>
                                                        {item.name}
                                                    </option>
                                                )
                                            })
                                        }
                                    </select>
                                </div>
                                <div className="mt-4">
                                    <Label forInput="delivery_address" value="Адрес доставки" />

                                    <Input
                                        type="text"
                                        name="delivery_address"
                                        value={data.delivery_address}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}
                                        required
                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="delivery_date" value="Срок доставки" />

                                    <Input
                                        type="text"
                                        name="delivery_date"
                                        value={data.delivery_date}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}
                                        required
                                    />
                                </div>
                                <div className="mt-4">
                                    <Label forInput="characteristics" value="Описание и требуемые функциональные, технические, качественные и эскплуатационные характеристики закупаемых товаров" />

                                    <Textarea
                                        name="characteristics"
                                        value={data.characteristics}
                                        className="mt-1 block w-full"
                                        handleChange={onHandleChange}
                                        required
                                        cols="4"
                                        placeholder="Дополнительное описание товара"
                                    />
                                </div>




                                <div className="flex items-center justify-end mt-4">
                                    <Button className="ml-4" processing={processing}>
                                        Сформировать
                                    </Button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </>

    );
}

CreateOrder.layout = (page) => (
    <UserLayout
        children = {page}
        auth={page.props.auth}
        errors={page.props.errors}
        header={<h2 className="font-semibold text-xl text-gray-800 leading-tight">Техническая спецификация закупаемых товаров к конкурсной документации</h2>}
        nav={<AsideNavbar />}
        notification={<div className="min-w-[50px] justify-around text-sm text-gray-400 flex flex-row items-center">
            <span className="material-icons-outlined">notifications</span>
        </div>}
    />
)
