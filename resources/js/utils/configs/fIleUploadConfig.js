// import pdfImage from '../../../../storage/app/public/images/fileUpload/file-pdf-red-240.png'
import pdfImage from '../../../../storage/app/public/images/fileUpload/pdf.png'
// import docImage from '../../../../storage/app/public/images/fileUpload/file-doc-blue-240.png'
import docImage from '../../../../storage/app/public/images/fileUpload/doc.png'
// import txtImage from '../../../../storage/app/public/images/fileUpload/file-txt-dark-240.png'
import txtImage from '../../../../storage/app/public/images/fileUpload/txt.png'
// import fileDefaultImage from '../../../../storage/app/public/images/fileUpload/file-gray-240.png'
import fileDefaultImage from '../../../../storage/app/public/images/fileUpload/default.png'

const getThumbnail = (name) => {
    return name.split('.').pop() ? name.split('.').pop() : 'default'
}

export default {
    getThumbnail: getThumbnail,
    pdf: pdfImage,
    doc: docImage,
    docx: docImage,
    txt: txtImage,
    default: fileDefaultImage
}
