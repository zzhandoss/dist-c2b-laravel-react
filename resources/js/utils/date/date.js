
const dateClass = function (datetime) {
    let date
    datetime ? date = new Date(datetime) : date = new Date()

    date.day = (date.getDate()<10?'0':'')+date.getDate()
    date.month = (date.getMonth()<10?'0':'')+date.getMonth()
    date.year = date.getFullYear()

    date.hours = (date.getHours()<10?'0':'')+date.getHours()
    date.minutes = (date.getMinutes()<10?'0':'')+date.getMinutes()
    date.seconds = (date.getSeconds()<10?'0':'')+date.getSeconds()

    date.fullDate = (separator) => {
        const sep = !!separator ? separator : '-'

        return date.day + sep + date.month + sep + date.year
    }

    date.fullTime = (separator) => {
        const sep = !!separator ? separator : ':'

        return date.hours + sep + date.minutes + sep + date.seconds
    }

    return date
}

export default dateClass
