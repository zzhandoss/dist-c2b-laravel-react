import {Fragment, useEffect, useState} from 'react'
import {Listbox, Transition} from '@headlessui/react'



function classNames(...classes) {
    return classes.filter(Boolean).join(' ')
}

export default function SelectBox({
    options,className,required,handleChange,isFocused,name
}) {
    const [selected, setSelected] = useState([])

    function someFunc(item){
        setSelected(item)
        handleChange(item)
    }

    return (
        <Listbox value={selected} onChange={ item => someFunc(item) }  required={required} isFocused={isFocused} name={name} >
            {({open}) => (
                <>
                    <div className={`mt-1 relative `}>
                        <Listbox.Button className={`relative w-full bg-white border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus:outline-none focus:ring-1 focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm` + className}>
                              <span className="flex items-center">
                                  <span className="ml-3 block truncate">{selected.name}</span>
                              </span>
                              <span className="ml-3 absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
                                <span className="material-icons-outlined">keyboard_arrow_down</span>
                              </span>
                        </Listbox.Button>
                        <Transition
                            show={open}
                            as={Fragment}
                            leave="transition ease-in duration-100"
                            leaveFrom="opacity-100"
                            leaveTo="opacity-0"
                        >
                            <Listbox.Options
                                className="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-56 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus:outline-none sm:text-sm">
                                <Listbox.Option
                                    disabled
                                    key="0"
                                    className={({active}) =>
                                        classNames(
                                            active ? 'text-white bg-indigo-600' : 'text-gray-900',
                                            'cursor-default select-none relative py-2 pl-3 pr-9'
                                        )
                                    }
                                    value={[]}
                                >
                                    {({selected, active}) => (
                                        <>
                                            <div className="flex items-center">
                                                    <span className={classNames(selected ? 'font-semibold' : 'font-normal', 'ml-3 block truncate')}>
                                                        Выберите категорию
                                                    </span>
                                            </div>
                                        </>
                                    )}
                                </Listbox.Option>
                                {options.map((item) => (
                                    <Listbox.Option
                                        key={item.id}
                                        className={({active}) =>
                                                        classNames(
                                                      active ? 'text-white bg-indigo-600' : 'text-gray-900',
                                                            'cursor-default select-none relative py-2 pl-3 pr-9'
                                                        )
                                                   }
                                        value={item}
                                    >
                                        {({selected, active}) => (
                                            <>
                                                <div className="flex items-center">
                                                    <span className={classNames(selected ? 'font-semibold' : 'font-normal', 'ml-3 block truncate')}>
                                                        {item.name}
                                                    </span>
                                                </div>
                                                {selected ? (
                                                    <span
                                                        className={classNames(
                                                            active ? 'text-white' : 'text-indigo-600',
                                                            'absolute inset-y-0 right-0 flex items-center pr-4'
                                                        )}
                                                    >
                                                        <span className="material-icons-outlined">check</span>
                                                    </span>
                                                ) : null}
                                            </>
                                        )}
                                    </Listbox.Option>
                                ))}
                            </Listbox.Options>
                        </Transition>
                    </div>
                </>
            )}
        </Listbox>
    )
}
