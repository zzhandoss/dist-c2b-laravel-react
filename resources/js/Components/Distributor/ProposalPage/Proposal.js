import React from 'react';

import fIleUploadConfig from "@/utils/configs/fIleUploadConfig";
import DateClass from "@/utils/date/date"
import {Inertia} from "@inertiajs/inertia";
import {InertiaLink} from "@inertiajs/inertia-react";
const Proposal = ({proposal},...props) => {
    const date = DateClass(proposal.created_at)
    const proposalDate = date.fullDate() + " | " + date.fullTime();

    function handleDownloadClick(path) {

    }
    return (
        <div className="w-11/12 mx-auto min-h-[70px] transition-all p-4 my-2 flex flex-col flex-wrap bg-white rounded-md shadow-md relative">
            <div className="flex flex-row justify-between text-lg font-bold text-gray-500 border-b">
                <div> Предложение от {proposalDate}

                </div>
            </div>
            {
                proposal.proposal_data && proposal.proposal_data.map(
                    proposalData =>
                        <div key={proposalData.id} className="flex flex-row items-center w-full mt-2">
                            <div key={proposalData.id} className="flex min-w-[30px] min-h-[30px] w-[30px] h-[30px] max-w-[50px] max-h-[50px]">
                                <img src={fIleUploadConfig[fIleUploadConfig.getThumbnail(proposalData.file)]} alt=""/>
                            </div>
                            <div className="flex w-full ml-2">{proposalData.filename}</div>
                            <div className="flex flex-row text-blue-400 items-center cursor-pointer">
                                <span className="material-symbols-outlined">download</span>
                                &nbsp;
                                <a href={route('download',{file: proposalData.file,filename: proposalData.filename})}>Загрузить</a>
                            </div>
                        </div>
                )
            }
        </div>
    );
};

export default Proposal;
