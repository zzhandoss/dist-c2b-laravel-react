import React from 'react';
import Proposal from "@/Components/Distributor/ProposalPage/Proposal";

const Proposals = ({proposals}) => {
    return (
        proposals && proposals.map(
            proposal =>
                <Proposal proposal={proposal} key={proposal.id} />)
    );
};

export default Proposals;
