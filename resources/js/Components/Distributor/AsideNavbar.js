import React from "react";
import AsideNavItem from "@/Components/AsideNavItem";

export default function AsideNavbar(
    {}
) {


    return (
        <div className="flex flex-col">
            <div className="py-2 border-b-gray-600 border-b">
                <AsideNavItem title='Новое предложение' icon='add' href={route('distributor_orders')} active={route().current('distributor_orders')} />
            </div>
            <AsideNavItem title='Мои предложения' icon='description' href={route('distributor_dashboard')} active={route().current('distributor_dashboard')} />
        </div>
    )
}
