import React from 'react';

const OrderCard = ({order}) => {
    return (
        <div className="w-11/12 mx-auto min-h-[150px] transition-all p-4 my-2 flex flex-col flex-wrap bg-white rounded-md shadow-md relative">
            <div className="flex flex-row justify-between text-lg font-bold text-gray-500 border-b">
                <div>
                    <span>{order.lot_name ?? order.lot_number}</span>
                    <span> | </span>
                    <span>{order.category}</span>
                </div>

                <span className="material-symbols-outlined select-none cursor-pointer hover:text-red-500">favorite</span>
            </div>
            <div className="flex flex-col md:flex-row text-gray-500 mt-2">
                <div className="flex flex-col w-2/3 pr-2">
                    <div className="flex flex-row">
                        <span className="font-bold">Номер лота:&nbsp;</span>
                        <span className=""> {order.lot_number}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Наименование заказа:&nbsp;</span>
                        <span className=""> {order.order_name}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Номер заказа:&nbsp;</span>
                        <span className=""> {order.order_number}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Описание лота:&nbsp;</span>
                        <span className=""> {order.lot_description}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Дополнительное описание:&nbsp;</span>
                        <span className=""> {order.lot_additional_description}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Характеристика:&nbsp;</span>
                        <span className=""> {order.characteristics}</span>
                    </div>
                </div>
                <div className="flex flex-col w-1/3 md:border-l px-2">
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Компания:&nbsp;</span>
                        <span className=""> {order.user.company}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Сотрудник:&nbsp;</span>
                        <span className=""> {order.user.name + " " + order.user.surname}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Создано:&nbsp;</span>
                        <span className=""> {order.created_at.slice(0,10)}</span>
                    </div>

                    <div className="flex flex-row flex-wrap mt-6 mb-2">
                        <span className="">Детали заказа</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Количество:&nbsp;</span>
                        <span className=""> {order.quantity + " " + order.unit_of_measure}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Адрес доставки:&nbsp;</span>
                        <span className=""> {order.delivery_address}</span>
                    </div>
                    <div className="flex flex-row flex-wrap">
                        <span className="font-bold">Дата доставки:&nbsp;</span>
                        <span className=""> {order.delivery_date}</span>
                    </div>
                </div>
            </div>

        </div>
    );
};

export default OrderCard;
