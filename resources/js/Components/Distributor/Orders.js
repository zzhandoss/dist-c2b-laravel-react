import React from 'react';
import Order from "@/Components/Distributor/Order";

const Orders = ({orders,user}) => {
    return (
            orders && orders.map(item =>
                <Order
                    key={item.id}
                    order={item}
                    user={item.user}
                />
            )
    );
};

export default Orders;
