import React, {useState} from 'react';

const Order = ({order,user}) => {
    const [isExpand, setIsExpand] = useState(false)
    return (
        <div className="order w-11/12 mx-auto min-h-[150px] overflow-hidden transition-all p-4 my-2 flex flex-col flex-wrap bg-white rounded-md shadow-md">
            <div className="flex flex-row flex-wrap place-self-start justify-between w-full border-b">
                <div className="flex flex-column sm:flex-row">
                    <span className="text-gray-500 text-lg font-bold">{order.lot_name} : {order.lot_number}&nbsp; </span>
                    <span className="hidden sm:flex items-start self-start">{"|"}&nbsp;</span>
                    <span className="flex items-center self-start text-gray-500 font-bold">
                        <span className="category-icon-sm material-symbols-outlined">sell</span>
                        &nbsp;
                        {order.category}
                    </span>
                </div>
                <div className="flex flex-wrap items-center">
                    <span className="text-gray-500 font-bold flex items-center">
                        <span className="calendar-month-icon-sm material-symbols-outlined">calendar_month</span>
                        &nbsp;Создана:&nbsp;
                    </span>
                    <span>{order.created_at.slice(0,10)}&nbsp;</span>
                    <span className="hidden sm:flex"> {"|"}&nbsp; </span>
                    <span>{user.company}</span>
                </div>
            </div>
            <div className="flex flex-col w-full self-start">
                <div className="my-2">
                    {order.lot_description}
                </div>
                <div className="flex flex-row mb-2">
                    <span className="text-gray-500 font-bold flex items-center">
                        <span className="numbers-icon-sm material-symbols-outlined">numbers</span>
                        Количество:&nbsp;
                    </span>
                    <span>{order.quantity + " " + order.unit_of_measure}</span>
                </div>
            </div>
            <div className="flex flex-row flex-col sm:flex-row sm:justify-between">
                <div className="flex flex-col sm:flex-row flex-wrap">
                    <div className="flex flex-row flex-wrap">
                        <span  className="text-gray-500 font-bold flex items-center">
                        <span className="event-icon-sm material-symbols-outlined">event</span>
                            &nbsp;Дата доставки:&nbsp;
                        </span>
                        <span>{order.delivery_date}&nbsp;</span>
                    </div>
                    <span className="hidden md:flex">{"|"}&nbsp;</span>
                    <div className="flex flex-row flex-wrap">
                        <span  className="text-gray-500 font-bold flex items-center">
                        <span className="map-icon-sm material-symbols-outlined">home_pin</span>
                            &nbsp;Адрес доставки:&nbsp;
                        </span>
                        <span>{order.delivery_address}</span>
                    </div>
                </div>
                <div
                    className="w-full sm:w-auto md:w-auto justify-end flex items-center self-end cursor-pointer text-gray-700 font-bold hover:text-gray-400 select-none"
                    onClick={e => setIsExpand(prevState => !prevState)}
                >
                    {isExpand ? "Свернуть" : "Развернуть"}
                    &nbsp;
                    <span className={`expand-more-icon-sm material-symbols-outlined transition duration-500 ${isExpand && "expand-more-icon-rotated"}`}>expand_more</span>
                </div>
            </div>
            {
                <div className={"transition-all duration-200 " + (!isExpand ? "min-h-0" : "min-h-[100px]")}>
                    {isExpand &&
                        <div className="transition-all animate-[orderFadeOn_0.5s] mt-2 flex flex-col">
                            <div className="flex flex-col md:flex-row flex-wrap md:items-center">
                                <div className="flex flex-row flex-wrap">
                                    <span className="text-gray-500 font-bold flex items-center">
                                        <span className="subtitles-icon-sm material-symbols-outlined">subtitles</span>
                                        &nbsp;Наименование закупки:&nbsp;
                                    </span>
                                    {order.order_name && order.order_name}&nbsp;
                                </div>
                                <span className="hidden md:flex">|&nbsp;</span>
                                <div className="flex flex-row flex-wrap">
                                    <span className="text-gray-500 font-bold flex items-center">
                                        <span className="pin-icon-sm material-symbols-outlined">pin</span>
                                        &nbsp;Номер закупки:&nbsp;
                                    </span>
                                    {order.order_number && order.order_number}
                                </div>
                            </div>
                            <div className="flex flex-col">
                                <span className="text-gray-500 font-bold flex items-center">
                                    <span className="feed-icon-sm material-symbols-outlined">feed</span>&nbsp;Дополнительное описание:
                                </span>
                                <span>
                                    {order.lot_additional_description}
                                </span>
                            </div>
                            <div className="flex flex-col">
                                <span className="text-gray-500 font-bold flex items-center">
                                    <span className="feed-icon-sm material-symbols-outlined">display_settings</span>&nbsp;Характеристики:
                                </span>
                                <span>
                                    {order.characteristics}
                                </span>
                            </div>
                            <div className="flex flex-row flex-wrap justify-between border-t mt-2">
                                <div className="flex flex-row flex-wrap text-gray-500 font-bold items-center">
                                    <span className="rate-review-icon-sm material-symbols-outlined">rate_review</span>&nbsp;Полученных предложений: 5
                                </div>
                                <div className="flex">
                                    <span className="flex items-center font-bold cursor-pointer text-sm text-sky-500">
                                        Перейти к предложениям
                                        <span className="double-arrow-icon-sm material-symbols-outlined">double_arrow</span>
                                    </span>
                                </div>
                            </div>
                        </div>
                    }
                </div>
            }


        </div>
    );
};

export default Order;
