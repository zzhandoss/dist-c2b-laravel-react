import React from "react";
import AsideNavItem from "@/Components/AsideNavItem";

export default function AsideNavbar(
    {}
) {


    return (
        <div className="flex flex-col">
            <div className="py-2 border-b-gray-600 border-b">
                <AsideNavItem title='Новая заявка' icon='add' href={route('order.create')} active={route().current('order.create')} />
            </div>
            <AsideNavItem title='Входящие' icon='article' href={route('customer_dashboard')} active={route().current('customer_dashboard')} />
            <AsideNavItem title='Мои заявки' icon='description' href={route('customer_out')} active={route().current('customer_out')} />
        </div>
    )
}
