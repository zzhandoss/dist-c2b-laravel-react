import React from 'react';
import Order from "@/Components/Customer/Order";

const Orders = ({orders,user}) => {
    return (
            orders && orders.map(item =>
                <Order
                    key={item.id}
                    order={item}
                    user={user}
                />
            )
    );
};

export default Orders;
