import React, {useContext} from "react";
import {AsideContext} from "@/Layouts/UserLayout";

export default function AsideFooter({}){
    const asideState = useContext(AsideContext)

    return (
        <div className={`absolute bottom-0 w-full p-2  flex flex-col flex-end text-gray-400 border-t border-t-gray-600 delay-500 ` + (!asideState ? ' transition-none opacity-0 ' : '')}>
            <span className="text-xs"> © 2022 ТОО "Cerebro Innovation Technologies", Все права защищены </span>
        </div>
    )
}
