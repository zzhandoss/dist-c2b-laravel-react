import React from 'react';
import TagListItem from "@/Components/SearchBar/TagListItem";

const TagList = ({suggestions,handleSuggestionClick}) => {



    return (
        <div className="box-scroll absolute top-11 flex flex-col w-full max-h-56 border p-1 rounded-md overflow-y-auto bg-white shadow-sm ">
        {
            suggestions.length > 0 ?
                suggestions.map(item => {
                    return (
                        <TagListItem
                            key={item.id}
                            item={item}
                            handleSuggestionClick={handleSuggestionClick}
                        />
                    )
                }) : <div className="flex items-center justify-center py-2 cursor-default">
                    Нет совпадений
                </div>
        }
        </div>
    )
}

export default TagList;
