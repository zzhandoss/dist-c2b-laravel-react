import React from 'react';
import TagSearchBar from "@/Components/TagSearchBar";

const OrderFilters = ({showFilters, filtersClassName, filters, categories, setFilters}) => {
    return (
        <>
            {showFilters &&
            <div className={`flex flex-row lg:flex-nowrap flex-wrap ` + (filtersClassName) }>
                <div className={`order-filter flex flex-col lg:max-w-[50%] sm:max-w-full w-1/2 grow mx-2 border-gray-400 border-b`}>
                        <span className="flex items-center">
                            <span className="category-icon-sm material-symbols-outlined">sell</span>
                            Категории:
                        </span>
                        <TagSearchBar
                            selectedTags={(categories) => setFilters({...filters,categories:categories} ) }
                            inputTags={categories}
                            defaultSelectedTags={filters.categories}
                            inputClassName="w-54"
                        />
                </div>
                <div className={`flex flex-row flex-wrap justify-between lg:w-[48.5%] sm:w-full grow `}>
                    <div className={`order-filter flex flex-col sm:max-w-full grow border-gray-400 border-b mx-2`}>
                        <span className="flex items-center">
                            <span className="numbers-icon-sm material-symbols-outlined">numbers</span>
                            Количество от:
                        </span>
                        <input name="quantity" className={`outline-none border-none bg-transparent h-8 focus:ring-0 m-1` } type="number" value={filters.quantity} min="0" onChange={(e) => setFilters({...filters,quantity:e.target.value})}/>
                    </div>
                    <div className={`order-filter flex flex-col sm:max-w-full grow border-gray-400 border-b mx-2`}>
                        <span className="flex items-center">
                            <span className="calendar-month-icon-sm material-symbols-outlined">calendar_month</span>
                            Дата от:
                        </span>
                        <input name="date" className={`outline-none border-none bg-transparent h-8 focus:ring-0 m-1` } type="date" value={filters.date} onChange={(e) => setFilters({...filters,date:e.target.value})}/>
                    </div>
                </div>
            </div>
            }
        </>
    );
};

export default OrderFilters;
