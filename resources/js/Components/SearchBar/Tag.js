import React from 'react';

const Tag = ({item,removeTag}) => {
    return (
        <li className={`w-auto h-8 flex items-center justify-center text-gray-600 p-2 bg-gray-200 rounded-md m-1 cursor-default`}>
            <span>{item.name}</span>
            <span onClick={() => removeTag(item.id)}
                  className="material-icons-outlined cancel-icon-sm ml-1 cursor-pointer">cancel</span>
        </li>
    );
};

export default Tag;
