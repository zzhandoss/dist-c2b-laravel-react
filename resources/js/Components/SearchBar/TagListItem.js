import React from 'react';

const TagListItem = ({item,handleSuggestionClick}) => {
    return (
        <div
             onClick={(e) => handleSuggestionClick(e,item)}
             className="flex items-center justify-center py-2 cursor-pointer  hover:bg-gray-200">
            {item.name}
        </div>
    );
};

export default TagListItem;
