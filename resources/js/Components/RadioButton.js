import React, { useEffect, useRef } from 'react';

export default function RadioButton({
  name,
  value,
  className,
  label,
  required,
  isFocused,
  handleChange,
  checked,
                              }) {
    const input = useRef();

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    return (
        <div className="flex flex-raw items-start">
            <input
                type={'radio'}
                name={name}
                value={value}
                className={
                    `border-gray-300 focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 rounded-md shadow-sm ` +
                    className
                }
                ref={input}
                required={required}
                id={"radio-"+value}
                onChange={(e) => handleChange(e)}
            />
            <label htmlFor={"radio-"+value}>{label}</label>
        </div>
    );
}
