import React, {useEffect, useRef, useState} from "react";
import Tag from "@/Components/SearchBar/Tag";
import DropdownInput from "@/Components/UI/DropdownInput/DropdownInput";

export default function TagSearchBar({
    inputTags,defaultSelectedTags,selectedTags,className,inputClassName
}){
    const dropdown = useRef()

    const [tags,setTags] = useState(defaultSelectedTags ?? [])
    const [suggestions,setSuggestions] = useState([tags])
    const [isPossibleToDelete,setIsPossibleToDelete] = useState(true)

    const suggestionsList = items => setSuggestions(items)
    const handleDropdownItemClick = (e,item) => addTag(item)

    function addTag(item){
        if(item){
            if(tags.indexOf(item) === -1){
                setTags([...tags,item])
                selectedTags([...tags,item])
            }
        }
        dropdown.current.clearInput()
    }
    function removeTag(idToRemove){
        const newTags = tags.filter((tag) => tag.id !== idToRemove )
        setTags(newTags)
        selectedTags(newTags)
    }
    function removeLastTag(){
        const newTags = tags.filter((_,index) => index !== tags.length-1)
        setTags(newTags)
        selectedTags(newTags)
    }
    function handleKeyDown(e){
        (e.key === 'Backspace' && e.target.value==='') ? setIsPossibleToDelete(true) : setIsPossibleToDelete(false)
    }
    function handleKeyUp(e){
        (e.key === 'Enter' && suggestions.length > 0) ? addTag(suggestions[0]) : (e.key === 'Backspace' && isPossibleToDelete && removeLastTag())
    }


    return (
        <div className={`tags-input flex flex-row items-start flex-wrap z-10 ` + className}>
            <ul className={`flex flex-wrap`}>
                {
                    tags.map((item,index) => {
                        return (
                            <Tag
                                key={item.id}
                                item={item}
                                removeTag={removeTag}
                            />
                        )
                    })
                }
                <DropdownInput
                        ref={dropdown}
                        items={inputTags}
                        selectedItems={tags}
                        suggestionsList={suggestionsList}
                        inputClassName={`min-w-60 outline-none border-none bg-transparent h-8 focus:ring-0 m-1 px-0 ` + inputClassName}
                        dropdownClassName={`flex flex-col ` }
                        handleDropdownItemClick={handleDropdownItemClick}
                        onKeyUp={handleKeyUp}
                        onKeyDown={handleKeyDown}
                        //( handleKeyUp(e))
                />
            </ul>
        </div>
    )
}
