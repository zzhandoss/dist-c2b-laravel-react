import React from 'react';

export default function Button({ type = 'submit', className = '', processing,onClick, children }) {
    return (
        <button
            type={type}
            className={
                `inline-flex items-center px-4 py-2 bg-cerebro border border-transparent rounded-md font-semibold text-xs text-white uppercase tracking-widest active:bg-cerebro transition ease-in-out duration-150 ${
                    processing && 'opacity-25'
                } ` + className
            }
            onClick={onClick}
            disabled={processing}
        >
            {children}
        </button>
    );
}
