import React from 'react';

const Paginator = ({paginator,from,to,total,search,href}) => {
    function handleLinkClick(e,pageNum){
        e.preventDefault()
        pageNum > 0 && pageNum < paginator.length-1 && search(href,pageNum)
    }

    return (
        <div className={`w-11/12 mx-auto mt-2` }>
            <div className="px-4 py-3 flex items-center justify-between border-t border-gray-200 sm:px-6">
                <div className="flex-1 flex justify-between sm:hidden">
                    <a href="#"
                       onClick={(e) => handleLinkClick(e,paginator[0].page_number)}
                       className={
                           paginator[1].active
                           ?
                               "relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-200 bg-white"
                           :
                               "relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                       }
                    >Previous</a>
                    <a href="#"
                       onClick={(e) => handleLinkClick(e,paginator[paginator.length-1].page_number)}
                       className="ml-3 relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50"
                    >Next</a>
                </div>
                <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                    <div>
                        <p className="text-sm text-gray-700">
                            Показано <span className="font-medium">с {from}</span> по <span className="font-medium">{to}</span> из{' '}
                            <span className="font-medium">{total}</span> результатов
                        </p>
                    </div>
                    <div>
                        <nav className="relative z-0 inline-flex rounded-md shadow-sm -space-x-px" aria-label="Pagination">
                            <a
                                href="#"
                                onClick={(e) => handleLinkClick(e,paginator[0].page_number)}
                                className={
                                    paginator[1].active
                                    ?
                                        "relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-300 cursor-default"
                                    :
                                        "relative inline-flex items-center px-2 py-2 rounded-l-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"
                                }
                            >
                                <span className="sr-only">Previous</span>
                                <span className="material-symbols-outlined">chevron_left</span>
                            </a>
                            {/* Current: "z-10 bg-indigo-50 border-indigo-500 text-indigo-600", Default: "bg-white border-gray-300 text-gray-500 hover:bg-gray-50" */}

                            {
                                paginator.length > 0 && paginator.map((link,index) =>
                                     ( link.url && index !== paginator.length -1 && index !== 0 ?
                                        <a
                                            key={index}
                                            href="#"
                                            onClick={(e) => handleLinkClick(e,Number(link.label))}
                                            aria-current={link.active && "page"}
                                            className={
                                                link.active
                                                ? `z-10 bg-slate-50 border-slate-500 text-slate-600 relative inline-flex items-center px-4 py-2 border text-sm font-medium`
                                                : `bg-white border-gray-300 text-gray-500 hover:bg-gray-50 relative inline-flex items-center px-4 py-2 border text-sm font-medium`
                                            }
                                        >
                                            {link.label}
                                        </a>
                                             : (index !== 0 && index !== paginator.length-1) && <span
                                                 key={index}
                                                 className="relative inline-flex items-center px-4 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-700">...</span>

                                    )
                                )
                            }

                            <a
                                href="#"
                                onClick={(e) => handleLinkClick(e,paginator[paginator.length-1].page_number)}
                                className={
                                    paginator[paginator.length -2].active
                                        ?
                                        "relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-300 cursor-default"
                                        :
                                        "relative inline-flex items-center px-2 py-2 rounded-r-md border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50"
                                }
                            >
                                <span className="sr-only">Next</span>
                                <span className="material-symbols-outlined">chevron_right</span>
                            </a>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Paginator;
