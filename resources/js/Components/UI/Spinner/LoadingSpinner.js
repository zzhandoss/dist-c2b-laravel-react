import React from 'react';
import classes from './LoadingSpinner.module.css'

const LoadingSpinner = () => {
    return (
        <div className={classes.ldsSpinner}>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
            <div></div>
        </div>
    );
};

export default LoadingSpinner;
