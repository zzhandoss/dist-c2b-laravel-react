import React from 'react';
import TagListItem from "@/Components/SearchBar/TagListItem";
import DropdownListItem from "@/Components/UI/DropdownInput/DropdownListItem";

const DropdownList = ({items,handleItemClick,dropdownListClassName}) => {
    return (
        <div className={`box-scroll absolute top-11 flex flex-col w-full max-h-56 border p-1 rounded-md overflow-y-auto bg-white shadow-sm ` + dropdownListClassName}>
            {
                items.length > 0 ?
                    items.map(item => {
                        return (
                            <DropdownListItem
                                key={item.id}
                                item={item}
                                handleDropdownItemItemClick={handleItemClick}
                            />
                        )
                    }) : <div className="flex items-center justify-center py-2 cursor-default">
                        Нет совпадений
                    </div>
            }
        </div>
    );
};

export default DropdownList;
