import React, {forwardRef, useEffect, useImperativeHandle, useMemo, useRef, useState} from 'react';
import DropdownList from "@/Components/UI/DropdownInput/DropdownList";
import useDropdownListSuggestions from "@/hooks/useDropdownListSuggestions";

const DropdownInput = forwardRef(({items,selectedItems,suggestionsList,inputClassName,dropdownClassName,handleDropdownItemClick,onKeyUp,onKeyDown,onChange,onFocus,onClick},ref) => {
    const input = useRef()

    const [showSuggestions,setShowSuggestions] = useState(false)
    const [q,setQ] = useState("")

    const suggestions = useDropdownListSuggestions(q,items,selectedItems,suggestionsList)

    function handleOnChange(e) {
        setQ(e.target.value)
        onChange && onChange(e)
    }
    function handleKeyUp(e){
        onKeyUp && onKeyUp(e)
    }
    function handleKeyDown(e){
        onKeyDown && onKeyDown(e)
    }
    function handleOnFocus(e){
        setShowSuggestions(true)
        onFocus && onFocus(e)
    }
    function handleOnClick(e){
        onClick && onClick(e)
    }

    useImperativeHandle(ref, () => ({
        clearInput(){
            setQ('')
        },
        closeSuggestions(){
            setShowSuggestions(false)
        }
    }))

    useEffect(()=> {
        const handleSomeClick = (e) => {
            input.current && e.target !== input.current && !input.current.contains(e.target) && setShowSuggestions(false)
        }
        document.addEventListener('click',handleSomeClick)
        return () => {
            document.removeEventListener('click',handleSomeClick)
        }
    }, [])

    return (
        <div className={`relative ` + (dropdownClassName)}>
            <input type="text"
                   ref={input}
                   value={q}
                   onKeyUp={e => handleKeyUp(e)}
                   onKeyDown={e => handleKeyDown(e)}
                   onChange={e => handleOnChange(e)}
                   onFocus={(e) => handleOnFocus(e)}
                   onClick={e => handleOnClick(e)}
                   className={inputClassName}
                   placeholder={'Начните вводить слово'}/>
            {showSuggestions &&
                <DropdownList items={suggestions}
                              handleItemClick={handleDropdownItemClick}

                />
            }
        </div>
    );
});

export default DropdownInput;
