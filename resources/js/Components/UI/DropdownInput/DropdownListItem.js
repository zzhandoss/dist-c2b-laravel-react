import React from 'react';

const DropdownListItem = ({item,handleDropdownItemItemClick}) => {
    return (
        <div
            onClick={(e) => handleDropdownItemItemClick(e,item)}
            className="flex items-center justify-center py-2 cursor-pointer  hover:bg-gray-200">
            {item.name}
        </div>
    );
};

export default DropdownListItem;
