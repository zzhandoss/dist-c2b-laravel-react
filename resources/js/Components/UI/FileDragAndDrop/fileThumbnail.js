import React from 'react';
import fileIcons from "@/utils/configs/fIleUploadConfig";

const FileThumbnail = ({files,removeFile}) => {

    const getThumbnail = (name) => {
        return name.split('.').pop() ? name.split('.').pop() : 'default'
    }

    const handleRemoveClick = (index) => {
        removeFile(index)
    }

    return (
            files && files.map((file,index) =>
            <div
                key={index}
                className="flex flex-row w-11/12 md:w-1/2 p-1 border-2 bg-gray-100 rounded-md z-10 m-1">
                <div className="flex min-w-[50px] min-h-[50px] h-[50px] w-[50px]">
                    <img src={fileIcons[getThumbnail(file.name)] || fileIcons['default']}
                         alt="file"
                         className="w-full h-full"
                    />
                </div>
                <div className="flex items-center w-full break-words ml-2">
                    <span>{file.name}</span>
                </div>
                <div className="flex items-center ">
                    <span
                        onClick={_ => handleRemoveClick(index)}
                        className="material-symbols-outlined text-red-600 cursor-pointer">delete</span>
                </div>
            </div>
        )

    );
};

export default FileThumbnail;
