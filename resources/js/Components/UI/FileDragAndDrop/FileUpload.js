import React, {useRef, useState} from 'react';

import FileThumbnail from "@/Components/UI/FileDragAndDrop/fileThumbnail";

const FileUpload = ({uploadFileCb,formats,count,savedFiles},...props) => {
    const [isDrag,setIsDrag] = useState(false)
    const [message,setMessage] = useState({
        show: false,
        text: null,
    })
    const dragInput = useRef(null)
    function showMessage(text,timeout){
        setMessage({
            show:true,
            text: text
        })
        setTimeout(() => setMessage({
            show:false,
            text: null
        }),timeout*1000)
    }

    function handleOnDragEnter(e){
        if(savedFiles?.length && savedFiles.length > 0 && savedFiles.length >= count) return
        setIsDrag(true)
    }
    function handleOnDragLeave(e){
        if(savedFiles?.length && savedFiles.length > 0 && savedFiles.length >= count) return
        setIsDrag(false)
    }
    function handleOnDragOver(e){
        if(savedFiles?.length && savedFiles.length > 0 && savedFiles.length >= count) return
        setIsDrag(true)
    }
    function handleDrop(e){
        if(savedFiles?.length && savedFiles.length > 0 && savedFiles.length >= count) return
        setIsDrag(false)
    }
    function handleFileInputChange(e){
        const files = e.target.files
        if(count < files.length){
            showMessage('Максимальное допустимое количесво файлов '+ count,2)
            return
        }
        if(formats &&
            Array.from(files).some(file => !formats.some(format => file.name.toLowerCase().endsWith(format.toLowerCase())))){
            showMessage('Допустимые форматы '+ formats.map(format => format),2)
            return
        }
        if(savedFiles.length >= count){
            showMessage('Максимальное число файлов достигнуто',2)
            return
        }

        uploadFileCb([...savedFiles,...files])
            // [...file,...files.map(file => file)]
    }
    function removeFile(index){
        const ul = [...savedFiles]
        ul.splice(index,1)
        uploadFileCb(ul)
    }

    return (
        <div
            ref={dragInput}
            onDragEnter={e => handleOnDragEnter(e)}
            onDragLeave={e => handleOnDragLeave(e)}
            onDragOver={e => handleOnDragOver(e)}
            onDrop={e => handleDrop(e)}
            className="flex mx-auto mt-2 py-6 w-11/12 min-h-[200px] border-dotted border-2 rounded-md shadow-lg shadow-inner relative z-10">
            <input
                className="opacity-0 absolute inset-0 w-[100%] h-[100%] cursor-pointer"
                type="file"
                onChange={e => handleFileInputChange(e)}
                value=""
                accept="application/pdf, application/msword, text/plain"
                multiple={true}
            />
                {
                    !(savedFiles?.length && savedFiles.length > 0 && !isDrag)
                        ?
                            !isDrag
                                ?
                                    <div className="flex flex-col justify-center m-auto">
                                <div className="flex flex-row font-bold">
                                    {
                                        !message.show
                                        ?
                                            <span>Перетащите сюда файл, либо нажмите, чтобы загрузить</span>
                                        :
                                            <span className="text-red-600">
                                                {message.text}
                                            </span>
                                    }
                                </div>
                                <div className="self-center mt-2  cursor-pointer">
                                    <div className="flex flex-row">
                                        <div className="flex w-[50px] h-[50px] border-2 border-blue-400">
                                            <span className="material-symbols-outlined text-blue-400 m-auto">note_add</span>
                                        </div>
                                        <div className="flex justify-center items-center text-blue-400 font-bold w-[200px] h-[50px] border-blue-400 border-y-2 border-r-2">
                                            <span>Загрузить файл</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                :
                                    <div className="flex justify-center m-auto font-bold">
                                <span>Отпускайте</span>
                            </div>
                        :
                            <div className="flex flex-col w-full justify-center items-center">
                                {message.show
                                    ?
                                        <span className="text-red-600 font-bold">{message.text}</span>
                                    :
                                        <span>Максимальное количество файлов: {count}</span>
                                }

                                <FileThumbnail
                                    removeFile={removeFile}
                                    files={savedFiles}
                                />
                            </div>
                }
        </div>
    );
};

export default FileUpload;
