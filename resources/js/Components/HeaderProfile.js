import React from 'react';
import { Fragment } from 'react'
import { Menu, Transition } from '@headlessui/react'
import {Link} from "@inertiajs/inertia-react";

export default function HeaderProfileMenu({ user }) {

    return (
        <Menu as="div" className="relative inline-block text-left">
            <div>
                <Menu.Button className="inline-flex justify-center w-full rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-sm font-medium text-gray-700 hover:bg-gray-50">
                    { user.name }
                    <span className="material-icons-outlined ml-2 account-box-icon-sm text-gray-400">account_box</span>
                </Menu.Button>
            </div>

            <Transition
                as={Fragment}
                enter="transition ease-out duration-100"
                enterFrom="transform opacity-0 scale-95"
                enterTo="transform opacity-100 scale-100"
                leave="transition ease-in duration-75"
                leaveFrom="transform opacity-100 scale-100"
                leaveTo="transform opacity-0 scale-95"
            >
                <Menu.Items className="origin-top-right absolute right-0 mt-2 w-44 rounded-md shadow-lg bg-white ring-1 ring-black ring-opacity-5 divide-y divide-gray-100 focus:outline-none">
                    <div className="py-1">
                        <Menu.Item>
                            {({ active }) => (
                                <Link
                                    href="#"
                                    className={` flex px-4 py-2 text-sm item-center ` + (active ? 'bg-gray-100 text-gray-900' : 'text-gray-700')}
                                >
                                    <span className="material-icons-outlined mr-2 account-circle-icon-sm text-gray-400">account_circle</span> Профиль
                                </Link>
                            )}
                        </Menu.Item>
                    </div>
                        <Menu.Item>
                            {({ active }) => (
                                <Link
                                    href={route('logout')}
                                    type="submit"
                                    method="post"
                                    as="button"
                                    className={` flex px-4 py-2 text-sm w-full text-left item-center ` + (active ? 'bg-gray-100 text-gray-900' : 'text-gray-700')}
                                >
                                    <span className="material-icons-outlined mr-2 logout-icon-sm text-gray-400">logout</span> Выйти
                                </Link>
                            )}
                        </Menu.Item>

                </Menu.Items>
            </Transition>
        </Menu>
    );
}
