import React, {useContext, useState} from "react";
import {AsideContext} from "@/Layouts/UserLayout";
import {Link} from "@inertiajs/inertia-react";
export default function AsideNavItem({
    icon,title,href,active
}){
    const asideState = useContext(AsideContext)

    return (
        <Link href={href}
            className={ `p-4 rounded flex flex-row hover:bg-slate-700 mt-2 ` +
        (
            active
            ? 'bg-slate-900'
            : ''
        )
        }>

                <span className={ `material-icons-outlined aside-nav-item-icon`} >{icon}</span>
                <span className={`aside-nav-item-title whitespace-nowrap duration-500 transition-all ` + ( !asideState ? 'opacity-0' : 'ml-2 duration-500 transition-all') }> {' ' + title}</span>

        </Link>
    )
}
