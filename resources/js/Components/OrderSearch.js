import React, {useEffect, useState} from "react";
import OrderFilters from "@/Components/SearchBar/OrderFilters";

export default function OrderSearch({
    categories, className, filtersClassName, setQuery, filters, setFilters, query
}){

    const [showFilters, setShowFilters] = useState(true)

    //const filteredOrders = useSearchedOrders(inputOrders,filters,q)

    function handleChange(e){
        setQuery(e.target.value)
    }

    useEffect(() => {
         setFilters(filters)
    },[filters])

    return(
        <div className={`flex flex-col w-full ` + (className)}>
            <div className={`flex flex-row`}>
                <input type="text"
                       value={query}
                       onChange={(e)=>handleChange(e)}
                       id="search-input"
                       placeholder={'Поиск'}
                       className={`w-full border-gray-300 focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 rounded-md shadow-sm` } />

                <div
                    onClick={() => setShowFilters(!showFilters)}
                    className={`flex text-gray-500 ml-2 p-2 cursor-pointer hover:bg-gray-300 rounded-md ` + (showFilters && "bg-gray-200")}>

                    <span className="material-symbols-outlined select-none">tune</span>
                </div>
            </div>
            <OrderFilters
                showFilters={showFilters}
                categories={categories}
                filtersClassName={filtersClassName}
                filters={filters}
                setFilters={setFilters}
            />

        </div>
    )
}
