import React from 'react';
import Logo from 'C:/OpenServer/domains/dist.c2b.local/storage/app/public/images/CerebroLogo.png'
export default function ApplicationLogo({ className }) {
    return (
        <img src={Logo} alt="Cerebro Innovation Technologies" className={className} />
    );
}
