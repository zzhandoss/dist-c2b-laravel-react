import React, { useEffect, useRef } from 'react';
import placeholder from "lodash/fp/placeholder";

export default function Input({
                                  name,
                                  value,
                                  className,
                                  autoComplete,
                                  required,
                                  isFocused,
                                  handleChange,
                                  cols,
                                  placeholder,
                              }) {
    const input = useRef();

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    return (
        <div className="flex flex-col items-start">
            <textarea
                cols={cols}
                name={name}
                value={value}
                className={
                    `border-gray-300 focus:border-blue-300 focus:ring focus:ring-blue-200 focus:ring-opacity-50 rounded-md shadow-sm ` +
                    className
                }
                ref={input}
                autoComplete={autoComplete}
                required={required}
                onChange={(e) => handleChange(e)}
                placeholder={placeholder}
            />
        </div>
    );
}
