<?php

use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',function (){})->middleware('auth')->middleware('redirect')->name('main');

Route::group(['middleware' => ['auth', 'customer'], 'namespace' => '\App\Http\Controllers\Customer', 'prefix' => 'customer'], function () {

    Route::get('/', 'HomeController@index')->name('customer_dashboard');
    Route::get('/out', 'HomeController@out')->name('customer_out');
    Route::resource('order','OrderController');
});

Route::group(['middleware' => ['auth', 'distributor'], 'namespace' => '\App\Http\Controllers\Distributor', 'prefix' => 'distributor'], function () {

    Route::get('/', 'HomeController@index')->name('distributor_dashboard');
    Route::get('/orders','HomeController@orders')->name('distributor_orders');
    Route::get('/order/{id}/proposal/create','ProposalController@create')->name('distributor_proposal.create');

    Route::post('/proposal','ProposalController@store')->name('distributor_proposal.store');
});

Route::group(['middleware' => ['auth', 'admin'], 'namespace' => '\App\Http\Controllers\Admin', 'prefix' => 'admin'], function () {

    Route::get('/', 'HomeController@index')->name('admin_dashboard');

});


Route::group(['middleware' => ['auth', 'distributor'], 'namespace' => '\App\Http\Controllers'], function () {
    Route::get('/download','HomeController@downloadFile' )->name('download');
});



require __DIR__.'/auth.php';
