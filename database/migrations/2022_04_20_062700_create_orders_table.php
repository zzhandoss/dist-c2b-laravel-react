<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('lot_category_id');
            $table->unsignedBigInteger('user_id');
            $table->integer('order_number')->nullable();
            $table->string('order_name')->nullable();
            $table->integer('lot_number')->nullable();
            $table->string('lot_name');
            $table->text('lot_description');
            $table->text('lot_additional_description')->nullable();
            $table->integer('quantity');
            $table->string('unit_of_measure');
            $table->string('delivery_address');
            $table->string('delivery_date');
            $table->text('characteristics');
            $table->foreign('lot_category_id')->references('id')->on('categories');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
