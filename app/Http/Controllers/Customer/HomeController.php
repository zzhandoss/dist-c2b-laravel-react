<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class HomeController extends Controller
{
    // '/' directory of Customer
    public function index(): \Inertia\Response
    {
        return Inertia::render('CustomerDashboard');
    }
    // '/out'
    public function out(Request $request): \Inertia\Response
    {
        $inputs = $request->all();
        $query = $inputs['q'] ?? '';
        $filters = isset($inputs['filters']) && $inputs['filters'] !== 'false' ? json_decode($inputs['filters']) : null;

        $orders = Order::whereLike($query)->withFilters($filters)->withCategoryName()->withCategoryId()->whereBelongsTo(Auth::user())->orderByDesc('created_at')->paginate(10);
        $paginatedOrders = $orders;
        $orders = $orders->makeHidden(['updated_at','user_id','lot_category_id']);
        $paginatedOrders->data = $orders;

        $categories = Category::all();
        $categories = $categories->makeHidden(['updated_at','created_at']);

        return Inertia::render('Customer/CustomerOut',['orders' => $paginatedOrders,'categories' => $categories]);
    }
}
