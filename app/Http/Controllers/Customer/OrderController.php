<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     *
     */
    public function index() : \Inertia\Response
    {
        $categories = Category::all();
        return Inertia::render("Customer/CreateOrder", ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() : \Inertia\Response
    {
        $categories = Category::all();
        return Inertia::render("Customer/CreateOrder", ['categories' => $categories]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'order_number' => 'numeric|nullable',
            'order_name' => 'string|nullable',
            'lot_number' => 'numeric|nullable',
            'lot_name' => 'string|required',
            'lot_category_id' => 'exists:App\Models\Category,id',
            'lot_description' => 'string|required',
            'lot_additional_description' => 'string|nullable',
            'quantity' => 'numeric|required',
            'unit_of_measure' => 'string|required',
            'delivery_address' => 'string|required',
            'delivery_date' => 'string|required',
            'characteristics' => 'string|required'
        ]);

        $request['user_id'] = Auth::user()['id'];

        Order::create($request->all());

        return redirect()->route('customer_dashboard')->with('success','Заявка успешно оставлена.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
