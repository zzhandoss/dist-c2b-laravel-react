<?php

namespace App\Http\Controllers\Distributor;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Proposal;
use App\Models\ProposalData;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Inertia\Inertia;
use Inertia\Response;

class ProposalController extends Controller
{

    function create($order_id) :Response {
        $order = Order::with('user')->withCategoryName()->findOrFail($order_id);
        $proposals = Proposal::
            whereBelongsTo(Auth::user())
            ->whereBelongsTo(Order::where('id',$order_id)->get())
            ->with('proposalData')
            ->latest()
            ->get();
        $proposals->makeHidden(['updated_at']);
        return Inertia::render('Distributor/DistributorProposalCreate',[
            'order' => $order,
            'proposals' => $proposals,
        ]);
    }

    function store(Request $request) {
        $files = $request->allFiles();
        $proposal = new Proposal;
        $proposal->user_id = Auth::user()->id;
        $proposal->order_id = $request->order_id;
        $proposal->save();

        foreach ($files['files'] as $file) {
            if($file->isValid()) {
                $filename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $fileext = $file->extension();
                $path = $file->storeAs('Distributors/Proposals/',$filename.'_'.time().'.'.$fileext,'public');
                $proposalData = new ProposalData;
                $proposalData->proposal_id = $proposal->id;
                $proposalData->file = $path;
                $proposalData->filename = $filename;
                $proposalData->save();
            }
        }

        return Redirect::route('distributor_proposal.create',['id'=>$request->order_id]);
    }


}
