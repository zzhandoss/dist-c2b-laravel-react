<?php

namespace App\Http\Controllers\Distributor;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Order;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Inertia\Inertia;

class HomeController extends Controller
{
     public function index(): \Inertia\Response
        {
            return Inertia::render('Distributor/DistributorDashboard');
        }

     public function orders(Request $request): \Inertia\Response
     {
         $inputs = $request->all();
         $query = $inputs['q'] ?? '';
         $filters = isset($inputs['filters']) && $inputs['filters'] !== 'false' ? json_decode($inputs['filters']) : null;

         $orders = Order::
            with('user')->
            whereLike($query)->
            withFilters($filters)->
            withCategoryName()->
            withCategoryId()->
            orderByDesc('created_at')->
            paginate(10);
         $paginatedOrders = $orders;
         foreach ($orders as $order) {
             $paginatedOrders->user = $order->user->makeHidden(['role']);
         }
         $orders = $orders->makeHidden(['updated_at','user_id','lot_category_id',]);
         $paginatedOrders->data = $orders;
         $categories = Category::all();
         $categories = $categories->makeHidden(['updated_at','created_at']);
         return Inertia::render('Distributor/DistributorOrders',['orders' => $paginatedOrders,'categories' => $categories]);
     }
}
