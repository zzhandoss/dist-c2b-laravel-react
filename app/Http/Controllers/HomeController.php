<?php

namespace App\Http\Controllers;

use GuzzleHttp\Psr7\MimeType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Inertia\Response;
use File;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

    }

    public function downloadFile(Request $request){
        $path = $request->file;


        $filename = $request->filename;

        $file = public_path('storage/'.$path);
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        $mimetype = MimeType::fromExtension($extension);

        if(file_exists($file)){
            return response()->download($file,$filename.'.'.$extension, ['Content-Type: '.$mimetype,]);
        }else {
            dd('1');
        }
    }

}
