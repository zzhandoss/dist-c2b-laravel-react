<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Proposal extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'user_id',
    ];

    function order(){
        return $this->belongsTo(Order::class,'order_id','id');
    }
    public function user() {
        return $this->belongsTo(User::class,'user_id','id');
    }

    function proposalData() {
        return $this->hasMany(ProposalData::class,'proposal_id','id');
    }
}
