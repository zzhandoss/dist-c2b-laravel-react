<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'order_number',
        'order_name',
        'lot_number',
        'lot_name',
        'lot_category_id',
        'lot_description',
        'lot_additional_description',
        'quantity',
        'unit_of_measure',
        'delivery_address',
        'delivery_date',
        'characteristics'
    ];

    public function user() {
        return $this->belongsTo(User::class,'user_id','id');
    }

    public function category() {
        return $this->belongsTo(Category::class,'lot_category_id','id');
    }
    public function proposals() {
        return $this->hasMany(Proposal::class,'order_id','id');
    }

//    public function scopeWithUserData($query) {
//        dd($this->user());
//        $query->addSelect([
//           'user_data' => User::where('id')
//        ]);
//    }

    public function scopeWithCategoryName($query) {
        $query->addSelect([
            'category' => Category::select('name')->whereColumn('lot_category_id','categories.id')->limit(1),
        ]);
    }

    public function scopeWithCategoryId($query) {
        $query->addSelect([
            'category_id' => Category::select('id')->whereColumn('lot_category_id','categories.id')->limit(1),
        ]);
    }

    public function scopeWhereLike($query,$request) {
        if ($request === '') return $query->select();
        return $query->where('lot_name','like','%'.$request.'%')->
            orWhere('lot_description','like','%'.$request.'%')->
                orWhere('lot_additional_description','like','%'.$request.'%')->
                    orWhere('lot_number','like','%'.$request.'%')->
                        orWhere('order_name','like','%'.$request.'%')->
                            orWhere('order_number','like','%'.$request.'%')->
                                orWhere('delivery_address','like','%'.$request.'%');
    }

    public function scopeWithFilters($query,$filters) {
        if ($filters === null) return $query->select();
        if(count( $filters->categories ) > 0) {
            foreach ($filters->categories as $category) $ids[] = $category->id;
            isset($ids) && $query->whereIn('lot_category_id', $ids);
        }
        if($filters->date !=='') $query->whereDate('created_at','>=',$filters->date);
        if($filters->quantity > 0) $query->where('quantity', '>=' ,$filters->quantity);
        return $query;
    }
}
