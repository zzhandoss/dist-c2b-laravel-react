<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProposalData extends Model
{
    use HasFactory;

    protected $fillable = [
        'proposal_id',
        'file',
        'filename'
    ];

    function propsal() {
        $this->belongsTo(Proposal::class,'proposal_id','id');
    }
}
